Fedora Design Team documentation
===================================

This repository contains the source content for the [Fedora Design Team documentation][1].
The Antora configuration files and AsciiDoc source content are hosted in this repository.
Please report Issues and submit Merge Requests for **content fixes** here.


## Contributing

The contents of this repository are licensed under the default content license of the Fedora Project.
Contributions are accepted under the default content license of the Fedora Project unless explicitly stated otherwise.
Contributors to this repository must have signed the [Fedora Project Contributor Agreement](https://docs.fedoraproject.org/en-US/legal/fpca/).


## Report an issue with the Design Team docs

Noticed some wrong or incorrect with the Design Team documentation?
Open a new Issue to report the problem.
Check the list of open issues first to see if someone else has already reported the problem before opening a new issue.

[**Fedora Design Team docs issues**][2]


## Local preview

This repo includes scripts to build and preview the contents of this repository.
Both scripts work on Fedora (using Podman) and macOS (using Docker).
To build and preview the site, run the following commands in a terminal:

```sh
cd /path/to/fedora/design/docs/
./docsbuilder.sh -p
```

A preview of local changes will be available at [localhost:8080](http://localhost:8080).

**NOTE**: If you `xref` to pages from other repositories, those links will not work in this local preview as it only builds this repository.
If you want to rebuild the whole Fedora Docs site, please see the [Fedora Docs build repository][3] for instructions.

### Installing Podman on Fedora

You may need to install Podman using the following command on Fedora Linux:

```sh
sudo dnf install podman
```

[1]: https://docs.fedoraproject.org/en-US/design/
[2]: https://gitlab.com/fedora/design/team/docs/-/issues
[3]: https://gitlab.com/fedora/docs/docs-website/docs-fp-o
